#!/bin/bash

# set -e
set -x
USAGE="Usage: $0 <tezos-opam-repo> <tezos-source-git> [<branch>]"

OPAMREPOSSH=${1?$USAGE}
TEZOSGIT=${2?$USAGE}
TARGETBRANCH=${3}
if [[ "$TARGETBRANCH" == "" ]]
then
    if [[ "$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME" != "" ]]
    then
        TARGETBRANCH="$CI_MERGE_REQUEST_TARGET_BRANCH_NAME-$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME"
    else
        TARGETBRANCH="$CI_COMMIT_REF_NAME"
    fi
fi

BRANCH=${CI_COMMIT_REF_NAME?$(git rev-parse --abbrev-ref HEAD)}

if [ "$#" -lt 2 ]; then
    echo "Illegal number of parameters"
    echo $USAGE
    exit 1
fi

if ! hash opam-ed 2>/dev/null; then
  opam remote add default http://opam.ocaml.org --rank=-1
  opam pin -y opam-ed https://github.com/AltGr/opam-ed.git
  opam install -y opam-ed
fi

REPOROOT="/tmp"
LOCAL_REPOROOT="${REPOROOT}/repo/${TARGETBRANCH}/"

TMPREPO=tmp.$$
git worktree add $TMPREPO $TARGETBRANCH

cd $TMPREPO


LONGHASH=$(git rev-parse HEAD)
if [[ $BRANCH == HEAD ]]
then
    BRANCH=branch-$(git rev-parse --short HEAD)
fi
VERSION=$(git log -1 --format=%cd --date=unix HEAD)-$BRANCH

cd -
git worktree remove $TMPREPO

rm -fr $LOCAL_REPOROOT
mkdir -p "${REPOROOT}/repo"
(cd "${REPOROOT}/repo" && \
  git clone "$OPAMREPOSSH" "$TARGETBRANCH" && \
  cd "$TARGETBRANCH" && \
  git checkout -B "$TARGETBRANCH")

if ! hash md5sum 2> /dev/null ; then
  # macOS compatibility
  function md5sum () {
    if (( $# == 0 )); then
      echo $(cat | md5)'  -'
    else
      for i in "$@" ; do
        echo $(md5 < "$i")"  $i"
      done
    fi
  }
fi

function f () {
    fullname=$1
    checksum=$2
    filename=$(basename "$fullname");
    dirname=$(dirname "$fullname");
    name="${filename%.*}";
    echo "Adding $fullname $name.$VERSION ..."

    pkgdir="$LOCAL_REPOROOT/packages/$name/$name.$VERSION"
    mkdir -p "$pkgdir"

    cp $fullname "$pkgdir"/opam
    echo -e "\n" >> "$pkgdir"/opam

    # lock all dev depedencies to a real version
    opam-ed -i 'map depends sed 's/"dev"/"'"$VERSION"'"/'' -f "$pkgdir/opam"

    # mv the install file since we use a tarball as source
    build_value="[\"mv\" \"$dirname/$name.install\" \".\"]"
    opam-ed -i "append build $build_value" -f "$pkgdir/opam"

    # fix the protocol compilation
    case "$name" in
      tezos-*protocol-*)
        protocol=${name##*-}
        echo "Amending $name : $pkgdir/opam"
        replace_field="[ \"%{tezos-protocol-compiler:lib}%/replace\" \"%{tezos-protocol-compiler:lib}%/dune_protocol.template\" \"dune\" \"$protocol\" ]"
        replace_value="[ \"%{tezos-protocol-compiler:lib}%/replace\" \"%{tezos-protocol-compiler:lib}%/dune_protocol.template\" \"$dirname/dune.inc\" \"$protocol\" ]"
        opam-ed -i "replace-item build $replace_field $replace_value" -f "$pkgdir/opam"
        ;;
    esac

    # add the url and checksum with the tarball
    url_value="{ src: \"$TEZOSGIT/-/archive/$LONGHASH/tezos-$LONGHASH.tar.gz\" checksum: \"$checksum\" }"
    echo -e "\nurl $url_value" >> "$pkgdir/opam"

}

curl -O $TEZOSGIT/-/archive/$LONGHASH/tezos-$LONGHASH.tar.gz

checksum=$(md5sum tezos-$LONGHASH.tar.gz | awk '{print $1}')
for i in src vendors ; do
  find $i -name '*.opam' | while read -r fullname; do
    f "$fullname" "$checksum"
  done
done
rm tezos-$LONGHASH.tar.gz

cd $LOCAL_REPOROOT

cat <<EOF >repo
opam-version: "2.0"
browse: "https://gitlab.com/tezos/tezos"
upstream: "https://gitlab.com/tezos/tezos"
announce: "Tezos opams repository"
EOF
echo "0.9.0" > version

opam admin index
opam admin cache
opam admin lint

git add *
git commit -a -m "$BRANCH - $(date)"
git push -u "$OPAMREPOSSH" "$BRANCH"
